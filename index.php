<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>

<html>
<head>
    <audio src="beep.mp3" autoplay ></audio> <!-- Sound notification of "ready to scan" -->
    <title>App Academy Inventory</title> 
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a><a href="logout.php" style="float: right">log out</a><br />
    </div>

  <div class="formWrapper">
      <?php
        if ($current_group === "teacher") {
            echo '<form action="teacherUI_selectDevice.php" method="post">';
        } else {
            echo '<form action="selectDevice.php" method="post">';
        }
        ?>
      
    <fieldset>
    <legend><?php echo '[' . $current_user . ']-[' . $current_group . '] '; ?>Scan a Device or Enter a Name: </legend>
        <div class="inventoryForm">
                <p><label class="field">Scan / Type: </label>
                <input type="text" name="deviceScan" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>
  </div>
	
  <!-- Extra Links for IT -->
  <?php
  
  if ($current_group === "IT") {
	echo '<div class="formWrapper">';
	echo '<form>';
	echo '<fieldset>';
	echo '<legend>[IT Functions]</legend>';
		echo '<h3>Inventory Status:</h3>';
		echo '<a href="browseAll.php" style="float: left">Browse All Inventory</a><br />';
		echo '<a href="browseChangeLog.php" style="float: left">Browse Change Log</a><br />';
		echo '<a href="ISS_status.php" style="float: left">Inventory Swap Station Status</a><br />';
		echo '<h3>Special Functions:</h3>';
		echo '<a href="massUpdate.php" style="float: left">Mass Update Devices</a><br />';
        echo '<a href="#" style="float: left">Swap Out a Computer</a><br />';
	echo '</fieldset>';
	echo '</form>';
	echo '</div>';
  }
  ?>
</body>
</html>            
