<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>
<html>
<head>
    <title>App Academy Inventory</title> 
    <link rel="stylesheet" type="text/css" href="css/tableStyles.css" />
</head>
<body>
<div class="banner">
    <a href="index.php"><img class="displayed" src="logo.png" /></a>
</div>
        
<?php

// sending query
$show_fields = "*";	
$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable");

if (!$result) {
    die("Query to show fields from table failed");
}

drawTableHorizontal($result);
mysqli_free_result($result);

?>

</body>
</html>
