<!-- This Page selects the device scanned from the previous page
     and provides a form for updating the location of that device -->

<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>
	 
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
    
<?php

$scan = strtoupper(filter_input(INPUT_POST, 'deviceScan'));

// allow browsing data for debugging etc
if ($scan === "*") {
     header("Location: browseAll.php");
} else {

// DETERMINE IF SCANNING DEVICE NUMBER, SERIAL NUMBER, STUDENT NAME
if (preg_match("/^\d{3,4}$/", "$scan", $devNum)) {
    $scan = "#" . "$devNum[0]";  // ensure variable $scan is in #NNN format
    $scanCriteria = "deviceNumber";    
} elseif (preg_match("/[A-z]+$/", $scan, $nameSearch)){
    // SCANNING BY STUDENT NAME
    $scanCriteria = "assignedTo";
} else {
    // SCANNING BY SERIAL NUMBER
    // Here I will need a regex to extract serial numbers from Lenovo Device Barcodes
    $scanCriteria = "serialNumber";
}

$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $scanCriteria = '$scan'");
$hits = mysqli_num_rows($result);
$getInfo = $result->fetch_array();
        if ($hits < 1) {
            die("<h1>Device Not Found!</h1>" . "<h2>Scanned for: " . $scan . " as " . $scanCriteria . "</h2></body></html>"
              . "<META HTTP-EQUIV='Refresh' Content='3; URL=index.php'>");
        }
        elseif ($hits > 1) {
            header('Location: multipleDevices.php');
        } else {
            // Convert Scan / ScanCriteria into DeviceNumber
            // so updates happen via unique key
            $scan = $getInfo['deviceNumber'];
            $scanCriteria = 'deviceNumber';
        }
        
$_SESSION['scanCriteria'] = $scanCriteria;
$_SESSION['scan'] = $scan;

if (isMissing()) {
    echo '<h1>This Device Has Been Reported as Missing or Stolen.</h1>';
    echo '<h2>Please Contact I.T. @ EXT 88523</h2>';
    go_home(3);
} else {

// DISPLAY THE FORM FOR SWAPPING THE DEVICE

if (strtoupper($getInfo['location']) === "STUDENT") {
$studentName = $getInfo['assignedTo'];
$_SESSION['studentName'] = $studentName;

echo '
    <div class="formWrapper">
    <form action="teacherUI_swapDevice.php" method="post">
    <fieldset>
    <legend>Scan replacement device:</legend>
        <div class="inventoryForm">
          <p><label class="field">New Device: </label>
          <input type="text" name="replacementDevice" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>';
echo "<h2>" . $studentName . "'s laptop: " . $scan . ", is going to REPAIR SHELF</h2>";
echo "<h2>Scan the new device " . $studentName . " will be using: </h2>";
// echo "<h2>If the device is not being replaced, scan the REPAIR SHELF code";
echo "<h3>Made a mistake? Wait a few seconds.  The screen will refresh and no changes will be made.</h3>";
}
echo "<h1>Current Info for: " . $scan . "</h1>";
drawTable($scanCriteria, $scan);
echo '</div>';

mysqli_free_result($result);
mysqli_close($con);
} // endif for checkIfStolen

} // endif for the 'secret inputs'
 
?>

</body>
</html>