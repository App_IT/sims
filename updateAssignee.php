<!-- This page takes the location scan from the previous page if it is
     "STUDENT" or "STAFF" and updates the assignee information for the 
     relevant device -->
<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>
	 
	 
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>

<?php
$assigneeInput = filter_input(INPUT_POST, 'assigneeInput');
$_SESSION['assigneeInput'] = $assigneeInput;

if (!mysqli_query($con, "UPDATE $inventoryTable "
        . "SET "
        . "location = '$locationScan', "
        . "assignedTo = '$assigneeInput' "
        . "WHERE $scanCriteria = '$scan'")) {
            die("Something went wrong! No info has been changed. \nError: " . mysqli_error($con));
} 

else {
    log_change($scanCriteria, $scan);

    // Grab updated info for visual verification
    $result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $scanCriteria = '$scan'") 
            OR die("Record updated, but soemthing went wrong trying to display the new info. \nError: " . mysqli_error($con));

echo '
    <div class="formWrapper">
    <form action="" method="">
    <fieldset>
    <legend>Verify the Information: </legend>
        <div class="inventoryForm">
        <p><label class="field">Please Check Info is Correct: </label>
        </div>
    </fieldset>
    </form>
    ';

    echo "<h1>New Info for: " . $scan . "</h1>";            
    drawTable($scanCriteria, $scan);
    echo '</div>';
    
    go_home(3);

}
mysqli_free_result($result);
mysqli_close($con);

?>

</body></html>