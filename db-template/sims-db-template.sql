-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 28, 2016 at 05:11 PM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app`
--
CREATE DATABASE IF NOT EXISTS `app` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `app`;

-- --------------------------------------------------------

--
-- Table structure for table `changelog`
--

CREATE TABLE IF NOT EXISTS `changelog` (
  `changeID` int(11) NOT NULL AUTO_INCREMENT,
  `timeStamp` datetime NOT NULL,
  `deviceNumber` varchar(6) NOT NULL,
  `category` varchar(6) DEFAULT NULL,
  `manufacturer` varchar(6) DEFAULT NULL,
  `serialNumber` varchar(8) DEFAULT NULL,
  `deviceName` varchar(15) DEFAULT NULL,
  `assignedTo` varchar(30) DEFAULT NULL,
  `location` varchar(32) DEFAULT NULL,
  `notes` varchar(54) DEFAULT NULL,
  `diskType` varchar(17) DEFAULT NULL,
  `model` varchar(14) DEFAULT NULL,
  `MAC1_eth` varchar(15) DEFAULT NULL,
  `MAC2_wifi` varchar(15) DEFAULT NULL,
  `user` varchar(64) NOT NULL,
  PRIMARY KEY (`changeID`),
  UNIQUE KEY `changeID` (`changeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5408 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `deviceNumber` varchar(4) NOT NULL DEFAULT '',
  `category` varchar(6) DEFAULT NULL,
  `manufacturer` varchar(6) DEFAULT NULL,
  `serialNumber` varchar(8) DEFAULT NULL,
  `deviceName` varchar(15) DEFAULT NULL,
  `assignedTo` varchar(64) DEFAULT NULL,
  `location` varchar(16) DEFAULT NULL,
  `notes` varchar(54) DEFAULT NULL,
  `diskType` varchar(17) DEFAULT NULL,
  `model` varchar(14) DEFAULT NULL,
  `MAC1_eth` varchar(15) DEFAULT NULL,
  `MAC2_wifi` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`deviceNumber`),
  UNIQUE KEY `deviceNumber` (`deviceNumber`),
  UNIQUE KEY `serialNumber` (`serialNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `locationID` int(11) NOT NULL AUTO_INCREMENT,
  `locationName` varchar(64) NOT NULL,
  `needsDetails` tinyint(1) NOT NULL,
  PRIMARY KEY (`locationID`),
  UNIQUE KEY `locationID` (`locationID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(64) NOT NULL,
  `group` varchar(64) NOT NULL,
  `passcode` varchar(256) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
