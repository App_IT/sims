<?php
require_once('include/init.php');
authorize("IT");
secure_page();

// Ensure Certain Session Variables are cleared because they are used by the next page to determine course of action
if (isset($_SESSION['massUpdateField'])) {
    unset($_SESSION['massUpdateField']);
    unset($massUpdateField);
}
if (isset($_SESSION['deviceList'])) {
    unset($_SESSION['deviceList']);
    unset($deviceList);
}
if (isset($_SESSION['scan'])) {
    unset($_SESSION['scan']);
    unset($scan);
}
if (isset($_SESSION['ignoredScans'])) {
    unset($_SESSION['ignoredScans']);
    unset($ignoredScans);
}

$loc_qry = mysqli_query($con, "SELECT locationName, needsDetails FROM $locationTable");
if (!$loc_qry) {
	die("Error with query");
}

$locations = array();
	while ($row = mysqli_fetch_assoc($loc_qry)){
		// Check if Details are needed and if not, allow mass update
		if ($row['needsDetails'] != 1) {
			$locations[] = $row['locationName'];
		}
	}
	// print_r($locations); /* debugging */
	mysqli_free_result($loc_qry);
	mysqli_close($con);
?>

<html>
<head>
    <title>App Academy Inventory</title> 
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>
<body>
        
<div id="banner">
    <a href="index.php"><img class="displayed" src="logo.png" /></a>
</div>
<h1>Work In Progress</h1>
<h2 style="color: red">If this page works at all, it may be buggy until this message is removed.</h2>
<h3><a href=index.php>Return to the Landing Page</a></h3>
  <div class="formWrapper">

    <form action="massUpdate-scan.php" method="post">   
    <fieldset>
    <legend><?php echo '[' . $current_user . ']-[' . $current_group . '] '; ?>Scan a Device or Enter a Name: </legend>
        <div class="inventoryForm">
            

<!-- Consider getting a list of all possible locations from the database via MySQL query here, 
     and listing them all then allowing or denying them on the next page. -->
		
                <p><label class="field">Mass Update Devices: </label>
                <ol>
				
				<?php
					foreach ($locations as $location) {
						echo '<li><button type="submit" value="' . $location . '" name="updateThis">' . $location . '</button></li><br />';
					}
				?>
                </ol>
        </div>
    </fieldset>
    </form> 
  </div>
    
</body>
</html>
