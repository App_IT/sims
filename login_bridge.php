<html>
<body>
<head>
    <title>App Academy Inventory</title> 
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>
<div class="banner">
    <a href="index.php"><img class="logo" src="logo.png" /></a>
</div>
  <div class="formWrapper">
    <form action="login_bridge.php" method="post">
    <fieldset>
    <legend>Login:</legend>
        <div class="inventoryForm">
<?php
require_once('include/init.php');

$loginScan = md5(filter_input(INPUT_POST, 'loginScan'));

// sending query
$result = mysqli_query($con, "SELECT * FROM $userTable WHERE passcode = '$loginScan'");

if (!$result) {
    // results are totally NULL (MySQL problem)
    echo "<h2>Could not connect to MYSQL.<h2>";
    go_login(5);
} else {
        $hits = mysqli_num_rows($result);
        // echo "<h2>Users Found: $hits</h2>";
	if ($hits < 1) {
	    // MySQL query was successful, but returned 0 rows, i.e., "User Not Found"
		echo "<h2>Couldn't login.<br /></h2>";
		echo "<h2>Hashed passphrase: $loginScan</h2>";  // Show hashed password for troubleshooting
	    go_login(3);
	}
        elseif ($hits > 1) {
            // Passphrase is not unique
            echo "<h2>Something went wrong logging in, too many results for one user<h2>";
            go_login(3);
        } else {
            // Passphrase was found, single result for users: Login.
            // echo "<h2>Found one user with $loginScan</h2>";
  	    $getInfo = $result->fetch_array();
            $current_user = $getInfo['userName'];
            $current_group = $getInfo['group'];
            $_SESSION['currentUser'] = $current_user;
            $_SESSION['currentGroup'] = $current_group;
            mysqli_free_result($result);
            go_home(1);
            echo '<p><label class="field">Logging in as ' . $current_user . '</label></p>';
        }
    }

?>
    </div>
    </fieldset>
    </form>
  </div>
</body>
</html>
