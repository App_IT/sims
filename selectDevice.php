<!-- This Page selects the device scanned from the previous page
     and provides a form for updating the location of that device -->

<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
    
<?php
$scan = strtoupper(filter_input(INPUT_POST, 'deviceScan'));

// allow browsing data for debugging etc
if ($scan === "*") {
     header("Location: browseAll.php");
} else {

// DETERMINE IF SCANNING DEVICE NUMBER, SERIAL NUMBER, STUDENT NAME
if (preg_match("/^\d{3,4}$/", "$scan", $devNum)) {
    $scan = "#" . "$devNum[0]";  // ensure variable $scan is in #NNN format
    $scanCriteria = "deviceNumber";    
} elseif (preg_match("/[A-z]+$/", $scan, $studentName)){
    // SCANNING BY STUDENT NAME
    $scanCriteria = "assignedTo";
} else {
    // SCANNING BY SERIAL NUMBER
    // Here I will need a regex to extract serial numbers from Lenovo Device Barcodes
    $scanCriteria = "serialNumber";
}

$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $scanCriteria = '$scan'");
$hits = mysqli_num_rows($result);
        if ($hits < 1) {
            die("<h1>Device Not Found!</h1>" . "<h2>Scanned for: " . $scan . " as " . $scanCriteria . "</h2></body></html>"
              . "<META HTTP-EQUIV='Refresh' Content='3; URL=index.php'><audio src='error_buzz.mp3' autoplay ></audio>");
        }
        elseif ($hits > 1) {
            header('Location: multipleDevices.php');
        } else {
            // Convert Scan / ScanCriteria into DeviceNumber
            // so updates happen via unique key
            $getInfo = $result->fetch_array();
            $scan = $getInfo['deviceNumber'];
            $scanCriteria = 'deviceNumber';
        }
        
$_SESSION['scanCriteria'] = $scanCriteria;
$_SESSION['scan'] = $scan;

if (isMissing()) {
    echo '<h1>This Device Has Been Reported as Missing or Stolen.</h1>';
    echo '<h2>Please Contact I.T. @ EXT 88523</h2>';
    echo '<audio src="error_buzz.mp3" autoplay ></audio>';
    go_home(3);
} else {

// DISPLAY THE FORM FOR UPDATING THE DEVICE'S LOCATION
echo '
    <div class="formWrapper">
    <form action="updateLocation.php" method="post">
    <fieldset>
    <legend>Scan Location Code to Update Location:</legend>
        <div class="inventoryForm">
          <p><label class="field">New Location: </label>
          <input type="text" name="locationScan" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>';
echo "<h1>Current Info for: " . $scan . "</h1>";
drawTable($scanCriteria, $scan);
echo '</div>';
mysqli_free_result($result);
mysqli_close($con);
} // endif for checkIfStolen

} // endif for the 'secret inputs'
?>

</body>
</html>
