<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>

<!-- This Page selects the device scanned from the previous page
     and provides a form for updating the location of that device -->

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
    
<?php


// allow browsing data for debugging etc
$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $scanCriteria = '$scan'");
        if (!mysqli_num_rows($result)) {
            die("<h1>Device Not Found!</h1>" . "<h2>Scanned for: " . $scan . "</h2></body></html>"
              . "<META HTTP-EQUIV='Refresh' Content='3; URL=index.php'>");
        }
        
        
// DISPLAY THE FORM FOR SELECTING A SINGLE DEVICE
echo '
    <div class="formWrapper">
    <form action="selectDevice.php" method="post">
    <fieldset>
    <legend>Multiple Devices Found.  Choose One:</legend>
        <div class="inventoryForm">
          <p><label class="field">Which Device?: </label>
          <input type="text" name="deviceScan" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>';
echo "<h1>Multiple Devices found for: " . $scan . "</h1>";
drawTable($scanCriteria, $scan);
echo '</div>';

mysqli_free_result($result);
mysqli_close($con);

?>

</body>
</html>