<?php
// RESET User and Group to force relogon if brought back to this screen
require_once('include/init.php');
if (isset($_SESSION)) {
	session_destroy();
}
?>
    
<html>
<head>
<title>App Academy Inventory</title> 
<link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>

<body>

    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
	<div>
		<h1> Logging out... </h1>
		<?php go_login(1); ?>
    </div>
</body>
</html>


</body>
</html>
