## Synopsis

SIMS is a **S**imple **I**nventory **M**anagement **S**ystem, coded in PHP/MySQL, meant to reduce inventory time cost and user error by using barcodes for almost every action, including login.

It was originally created for use by App Academy IT Staff at Pasadena High School to quickly and accurately track laptops and other devices through various users and locations.

It is possible to use this system as a standalone system accessible only on the machine it’s installed on, or as "network appliance."  As a network appliance, you can use a spare laptop, tablet, phone, or any other device on your local network to update inventory.

The setup is the same, except:

##### For the Standalone Version:
  1. You should install Ubuntu Desktop instead of Ubuntu Server
##### For the Network Appliance Version:
  1. You can use Ubuntu Server (particularly if using a VM) to minimize resource use.
  2. You’ll probably want to set a static IP address for your server.
  3. It’s more important to set strong passwords for MySQL and as SIMS Login Codes.

*Note: Due to security concerns, it is **NOT RECOMMENDED** to set up SIMS as an Internet-accessible service (on AWS, for example)*

## Installation

##### Install Ubuntu (Desktop or Server) 16:
  1. Choose LAMP server and (optionally) OpenSSH server.
  2. Choose a password for the MySQL root user and note it somewhere.
  3. Update the SIMS initDB.php file to reflect the MySQL password `$ sudo nano /var/www/html/sims/include/initDB.php`
  4. Install phpmyadmin `$ sudo apt-get install phpmyadmin`

##### Clone the SIMS from bitbucket.org
  1. `cd /var/www/html`
  2. `sudo git clone https://bitbucket.org/App_IT/sims`

##### Check that you see the SIMS screen:
  1. Ubuntu Desktop: Point a browser to `localhost/sims`
  2. Ubuntu Server: On another machine, point a browser to `<SIMS machine IP>/sims`

*Note: You will not be able to login until the database is imported (instructions follow)*

##### [ Optional ] Change the webroot to the SIMS directory
  1. Edit the apache2 default configuration file: `$ sudo nano /etc/apache2/sites-available/000-default.conf`
  2. Change `DocumentRoot /var/www/html` to `DocumentRoot /var/www/html/sims`
  3. Reload apache: `$ sudo service apache2 restart`

## Setup

##### Create the “app” database
  1. Point a browser to `localhost/phpmyadmin` or `<SIMS server ip>/phpmyadmin`
  2. Log in to phpmyadmin as root using the MySQL root password
  3. At the top of the phpmyadmin screen, click “Import”
  4. Import the sims-db-template.sql file

##### [ Optional ] Check that SIMS is working
  1. Point a browser to `localhost` or `<SIMS server ip>/sims`
  2. Type "admin" as the login code
  3. If you're able to login, everything should be working!

##### [ Optional ] Further testing
  4. Type "100" and see that the sample data for Device #100 is shown.
  5. Hit the Browser's "back" button or click the App Academy Logo to return to the home page.
  5. Click "Browse All Inventory" and check that the sample inventory is shown.

*If you've created this system in a Virtual Machine, now is a great time to take a snapshot for backup purposes!*

## Populating Tables with (Your) Data

* The “app” database will appear on the left, click on it to reveal the tables
* Additions and customizations can be made via phpmyadmin by clicking on the “app” database, then clicking on the relevant table.
* Add users/locations/inventory with “insert” toward the top of the page.
* Edit info by double-clicking the field you want to change.   
    
###### The User Table

* In the users table, there are two default users, “admin” and “teacher.”
* The system login passwords are the same as the username, i.e., “admin” and “teacher”
* To change a password, generate the md5 hash of the phrase you want and store it in the “passcode” field.*
* The system currently only supports “IT” and “teacher” as groups.*
  
###### The Locations Table
  
* Add locations to the location table.  There are two defaults inserted as examples.
* Setting “needsDetails” to 1 (true) will prevent a location from showing up in the “Mass Update” function.

###### The Inventory Table

Add your inventory to the inventory database.

* Be sure to place a “#” in front of the device number.  
* The Device Number must be formatted #NNN or #NNNN where Ns are numbers (e.g., #101 or #1023).
* The Device Number and Serial Number *must be unique* for each device
* It is possible to prepare your data in a CSV file and upload it to the table.
    1. Enter your data in the same format as appears in the inventory table in MySQL/PHPMyAdmin.
    2. Click on the inventory table, then toward the top, click “Import.”
    3. Ensure the page reports it is “Importing into the table ‘inventory’” at the top.
    4. Use the “Choose File” button to select your CSV file.
    5. Change Format from SQL to CSV.
    6. Click the “Go” button.


##### HOORAY!

The App Academy **S**imple **I**nventory **M**angement **S**ystem should now be working.

##### Refer to the Usage Guide for how to use the SIMS.
1. [Usage Guide](https://bitbucket.org/App_IT/sims/src/6f5ba613b6d1fe18b623abd91e07784fb08c4734/docs/sims_usage_guide.md?at=master) in this repo's [docs directory](https://bitbucket.org/App_IT/sims/src/6f5ba613b6d1fe18b623abd91e07784fb08c4734/docs/?at=master)
  
## License

Open Source / MIT License