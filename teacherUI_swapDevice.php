<!-- This page takes the location scan from the previous page if it is
     "STUDENT" or "STAFF" and updates the assignee information for the 
     relevant device -->
<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>
	 
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>

<?php

$replacementDevice = filter_input(INPUT_POST, 'replacementDevice');
if (preg_match("/^\d{3,4}$/", "$replacementDevice", $devNum)) {
    $replacementDevice = "#" . "$devNum[0]";  // ensure variable $scan is in #NNN format
    $replacement_scanCriteria = "deviceNumber";  
} else {
    // SCANNING BY SERIAL NUMBER
    // Here I will need a regex to extract serial numbers from Lenovo Device Barcodes
    $replacement_scanCriteria = "serialNumber";
}
// Check that the replacement device is valid
$result = mysqli_query($con, "SELECT * FROM $inventoryTable WHERE $replacement_scanCriteria = '$replacementDevice'");
$hits = mysqli_num_rows($result);
// Ensure the replacement device exists...
if ($hits < 1) {
    echo "<h1>The replacement device was not found.  Try another device.  Please set this device aside and contact IT.</h1>";
    echo "<h2>Scanned for: " . $replacementDevice . " as " . $replacement_scanCriteria . "</h2>";
    go_home(5);
    die();
} else {
$getInfo = $result->fetch_array();
$checkAssigned = $getInfo['assignedTo'];
if ($checkAssigned != "") {
    echo "<h1>The replacement device is already assigned to " . $checkAssigned . "</h1>";
    echo "<h2>Please try another device and return this device to IT.</h2>";
    go_home(5);
    die();
} else {

// Update replacement device with student's name and "student" location
if (!mysqli_query($con, "UPDATE $inventoryTable "
        . "SET "
        . "location = 'STUDENT', "
        . "assignedTo = '$studentName' "
        . "WHERE $replacement_scanCriteria = '$replacementDevice'")) {
            die("Something went wrong assigning the new device to the student! No info has been changed. \nError: " . mysqli_error($con));
} 
log_change($replacement_scanCriteria, $replacementDevice);
// Clear student's name form previous device
if ("$current_group" === "IT") {
	$repairLocation = "REPAIR";
} else {
	$repairLocation = "ISS_REPAIR($current_user)";
}
if (!mysqli_query($con, "UPDATE $inventoryTable "
        . "SET "
        . "location = '$repairLocation', "
        . "assignedTo = '' "
        . "WHERE $scanCriteria = '$scan'")) {
            die("Something went wrong sending device to repair shelf! Please note old and new devices and email IT. \nError: " . mysqli_error($con));
} 
log_change($scanCriteria, $scan);    

    // Grab updated info for visual verification
    //$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $scanCriteria = '$scan'") 
    //        OR die("Record updated, but soemthing went wrong trying to display the new info. \nError: " . mysqli_error($con));

echo '
    <div class="formWrapper">
    <form action="" method="">
    <fieldset>
    <legend>Verify the Information: </legend>
        <div class="inventoryForm">
        <p><label class="field">Please Check Info is Correct: </label>
        </div>
    </fieldset>
    </form>
    ';

    echo "<h1>Device " . $scan . " is now in Repair</h1>";            
    drawTable($scanCriteria, $scan);
    echo "<h1>" . $studentName . "'s new device is: " . $replacementDevice . "</h1>";            
    drawTable($replacement_scanCriteria, $replacementDevice);
    echo '</div>';
    
    go_home(10);

mysqli_free_result($result);
mysqli_close($con);
} // endif for checkAssigned
} // endif for ensure MYSQL result
?>

</body></html>
