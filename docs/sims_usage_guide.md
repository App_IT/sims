## **S**imple **I**nventory **M**anagement **S**ystem

This document outlines the process for updating inventory:

##### Inventory Update Process:
1. Scan an ID or ID code to Login (not yet implemented)
2. Scan the Device # Barcode or Serial Number Barcode or enter by hand
  * Information about the device is displayed on the screen
  * Ensure the selected device is correct (shows correct assignee, etc)
3. To update the device�s location, scan a Location Barcode or type in a location name (e.g., "STUDENT", "STAFF", "IT ROOM")
  * If the new Location is �Student� or �Staff�, you will have to assign the device to someone.
  * Type the assignee�s Full Name.
  * If you do not assign the device to a person, the inventory record will remain unchanged.
  *	The updated information is now displayed
4. Double-check the Location and Assignee (if any) is correct.
5. You will automatically be redirected back to the home page (Device Scanning)
  * There is no way to undo your change, but if something went wrong, simply rescan the device and go through this process again using the correct data.

##### Device Swap Process
1. Student notifies Staff they need a replacement laptop
2. Staff scans the barcode to log in at the Device Swapping Station (coded as Inventory Swap Station, or ISS)
3. Staff scans the problem device, and places it in the "For Repair" area.
4. Staff removes a fresh device from the "Device Cache" and scans it.
  * The Inventory System automatically assigns the name of the problem device to the name of the second device.
  * The problem device *must be already be assigned to location "STUDENT"* or the system will not allow the swap.


