<?php
// MAKE THE LIST LOOK NICE - OK
// Button for Commiting Changes
// Scan Code or something to commit changes
// Check for duplicate entries? (not very important)

require_once('include/init.php');
authorize("IT");
secure_page();
?>
	 
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>

<?php
if (!isset($_SESSION['massUpdateField'])) {
//      echo '<h2>POST part is getting activated</h2>';
        $massUpdateField = filter_input(INPUT_POST, 'updateThis');
//      echo '<h2>' . $massUpdateField . '</h2>';
        $_SESSION['massUpdateField'] = $massUpdateField;
	} else {
    
$scan = strtoupper(filter_input(INPUT_POST, 'deviceScan'));
// DETERMINE IF SCANNING DEVICE NUMBER, SERIAL NUMBER, STUDENT NAME
if (preg_match("/^\d{3,4}$/", "$scan", $devNum)) {
    $scan = "#" . "$devNum[0]";  // ensure variable $scan is in #NNN format
    $scanCriteria = "deviceNumber";    
} elseif (preg_match("/[A-z]+$/", $scan, $studentName)) {
    // SCANNING BY STUDENT NAME
    echo "<h2>This looks like a name.  Scan ignored.</h2>";
    $ignoreScan = true;
} else {
    // SCANNING BY SERIAL NUMBER
    $scanCriteria = "serialNumber";
}

if (!isset($ignoreScan)) {
$result = mysqli_query($con, "SELECT * FROM $inventoryTable WHERE $scanCriteria = '$scan'");
$hits = mysqli_num_rows($result);
$getInfo = $result->fetch_array();
    
        if ($hits < 1) {
            echo "<h1>Device Not Found!</h1>" . "<h2>Scanned for: " . $scan . " as " . $scanCriteria . "</h2>";
            $ignoreScan = true;            
        }
        elseif ($hits > 1) {
            echo '<h2>More than one device found for this scan.  Please report the issue to IT.</h2>';
            $ignoreScan = true;
        } else {
            // Convert Scan / ScanCriteria into DeviceNumber
            // so updates always happen via unique key
            $scanCriteria = 'deviceNumber';
            $scan = $getInfo['deviceNumber'];

        }

mysqli_free_result($result);
mysqli_close($con); 
}

// Make sure the device isn't supposed to be MISSING or STOLEN    
$deviceLocation = $getInfo['location'];
if ($deviceLocation == 'STOLEN' || $deviceLocation == 'MISSING') {
    echo '<h2>This device is listed as ' . $deviceLocation .  '. Please notify IT.</h2>';
    $ignoreScan = true;
}
    
if (!isset($ignoreScan)) {
    $_SESSION['deviceList'][] = $scan;
    echo '<audio src="beep.mp3" autoplay ></audio>';
} else {
    echo '<h2>This scan ignored.</h2>';
    $_SESSION['ignoredScans'][] = $scan;
    echo '<audio src="error_buzz.mp3" autoplay ></audio>';
    // could change to ignoredScans['scan'] = $scan AND
    // ignoredScans['reason'] = $reason if it's useful
}
if (isset($_SESSION['deviceList'])) {
    $deviceList = $_SESSION['deviceList'];
}
if (isset($_SESSION['ignoredScans'])) {
    $ignoredScans = $_SESSION['ignoredScans'];
}
}
    
echo '
    <div class="formWrapper">
    <form action=' . htmlspecialchars($_SERVER["PHP_SELF"]) . ' method="post">
    <fieldset>
    <legend>Scan Devices to Update to: ' . $massUpdateField . ' </legend>
        <div class="inventoryForm">
          <p><label class="field">Scan a Device: </label>
          <input type="text" name="deviceScan" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>';
if (isset($scan) && !isset($ignoreScan)) {
echo "<h1>Current Info for: " . $scan . "</h1>";
drawTable($scanCriteria, $scan);
}

// Show a count of how many entires will be updated
if (isset($deviceList)) {
    $deviceCount = count($deviceList);
    echo '<h2>' . $deviceCount . ' devices will be updated.</h2>';
    echo '<h2><a href="massUpdate-commit.php">Commit these devices to ' . $massUpdateField . '</a></h2>';
}
    
// Show a detailed list of which devices are going to be updated
if (isset($deviceList)) {
echo '<div id="updateList" style="float: left; display: inline; width: 50px;">';
echo '<h3>To be updated:</h3>';
echo '<ol>';
    foreach ($deviceList as $device) {
        echo '<li>' . $device . '</li>';
        }
    echo '</ol>';
    echo '</div>'; // end updateList
}    

// Show a list of entries that won't be updated
if (isset($ignoredScans)) {
echo '<div id="ignoreList" style="float: right; display: inline; width: 50px;">';
echo '<h3>Ignored Entries:</h3>';
echo '<ol>';
    foreach ($ignoredScans as $ignored) {
        echo '<li class = "error">' . $ignored . '</li>';
        }
    echo '</ol>';
    echo '</div>'; // end ignoredList
}

    
    
echo '</div>'; // end formWrapper?

?>

</body></html>
