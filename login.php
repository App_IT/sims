<?php
// RESET User and Group to force relogon if brought back to this screen
if (isset($_SESSION)) {
	session_destroy();
}
?>

<html>
<head>
<title>App Academy Inventory</title> 
<link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>

<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>

  <div class="formWrapper">
    <form action="login_bridge.php" method="post">
    <fieldset>
    <legend>Login:</legend>
        <div class="inventoryForm">
          <p><label class="field">Login: </label>
             <input type="password" name="loginScan" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>
  </div>

</body>
</html>


</body>
</html>
