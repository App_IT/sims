<?php

$db_host = '127.0.0.1';
$db_user = 'root';
$db_pwd = 'root';
$database = 'app';
$inventoryTable = 'inventory';
$locationTable = 'locations';
$userTable = 'users';
$changeLog = 'changelog';
$con = mysqli_connect($db_host, $db_user, $db_pwd, $database);

$show_fields = "deviceNumber, serialNumber, location, assignedTo, notes";
