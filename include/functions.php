<?php

//////////
// TABLES
//////////
function drawTableHorizontal($mysql_result) {
    
    $fields_num = mysqli_num_fields($mysql_result);

    // printing table headers
    echo "<table class='inventoryTable'><tr>"; 

    for($i=0; $i<$fields_num; $i++)
        {
            $field = mysqli_fetch_field($mysql_result);
            echo "<td>{$field->name}</td>";
        }
    echo "</tr>\n";
        while($row = mysqli_fetch_row($mysql_result))
        { 
            echo "<tr>";

            // $row is array... foreach( .. ) puts every element
            // of $row to $cell variable
            foreach($row as $cell)
            {
                echo "<td>$cell</td>";
            }
            echo "</tr>\n";
        } 
}

function drawTable($passed_scanCriteria, $passed_scan) {
    
    require('include/initDB.php');
    
    $mysqli_result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE $passed_scanCriteria = '$passed_scan'") or die(mysqli_error($con));
    $deviceHits = mysqli_num_rows($mysqli_result);
    $deviceNum = 1;
    while ($fieldName = mysqli_fetch_field($mysqli_result)) {
        $field[] = $fieldName->name;
    }
     
    while ($row = mysqli_fetch_row($mysqli_result)) {
        $index = 0;
//      echo "<h2 class='centered'>Device $deviceNum of $deviceHits</h2>";
        echo "<table class='previewTable'>"; 
            foreach ($row as $info) {                      
                echo "<tr>";
                echo "<td class='fieldName'>" . $field[$index] . ": </td>";
                echo "<td class='fieldValue'>$info</td>";
                echo "</tr>";
                $index++;                
            }
            $deviceNum++;
        echo "</table>";
        }
}

////////////
// DATABASE
////////////

function log_change($passed_scanCriteria, $passed_scan) {

require('include/initDB.php');
require('include/sessionVars.php');

$mysqli_result = mysqli_query($con, "SELECT * FROM $inventoryTable WHERE $passed_scanCriteria = '$passed_scan'")
        OR die(mysqli_error($con));

$row = mysqli_fetch_array($mysqli_result);
    $timeStamp = date('Y-m-d H:i:s');
    $deviceNumber = $row['deviceNumber'];
    $category = $row['category'];
    $manufacturer = $row['manufacturer'];
    $serialNumber = $row['serialNumber'];
    $deviceName = $row['deviceName'];
    $assignedTo = $row['assignedTo'];
    $location = $row['location'];
    $notes = $row['notes'];
    $diskType = $row['diskType'];
    $model = $row['model'];
    $MAC1_eth = $row['MAC1_eth'];
    $MAC2_wifi = $row['MAC2_wifi'];
    
    $insert_sql = "INSERT INTO $changeLog "
                . "(changeID,timeStamp,deviceNumber,category,manufacturer,"
                . "serialNumber,deviceName,assignedTo,location,notes,"
                . "diskType,model,MAC1_eth,MAC2_wifi,user) "
            
                . "VALUES ('NULL','$timeStamp','$deviceNumber','$category','$manufacturer',"
                . "'$serialNumber','$deviceName','$assignedTo','$location','$notes',"
                . "'$diskType','$model','$MAC1_eth','$MAC2_wifi','$current_user')";
    
    if (!mysqli_query($con, $insert_sql))
    {
        echo "<h1>Update Successful but Changes Not Logged</h1>";
        echo "<p>$timeStamp $deviceNumber $category "
                . "$manufacturer $serialNumber $deviceName "
                . "$assignedTo $location $notes "
                . "$diskType $model "
                . "$MAC1_eth $MAC2_wifi</p>";
        die(mysqli_error($con));
    }
    
}

///////////////////////
// SITE NAVIGATION
///////////////////////

function go_home($after_seconds) {
    if (!isset($after_seconds)) {
        $after_seconds = '5'; // set a default of 5 seconds in case I forget to specify a time
    }   
    echo "<META HTTP-EQUIV='Refresh' Content='$after_seconds; URL=index.php'>";
}

function go_login($after_seconds) {
    if (!isset($after_seconds)) {
        $after_seconds = '5'; // set a default of 5 seconds in case I forget to specify a time
    }   
    echo "<META HTTP-EQUIV='Refresh' Content='$after_seconds; URL=login.php'>";
}

///////////////////////
// DEVICE LOOKUP
///////////////////////

function validLocation() {
    require('include/initDB.php');
    require('include/sessionVars.php');
    $validLocation = mysqli_query($con, "SELECT * FROM $locationTable WHERE locationName = '$locationScan'");

    if (mysqli_num_rows($validLocation) === 0) {
        return FALSE;
            } else { return TRUE; }
    }

function isMissing() {
    require('include/initDB.php');
    require('include/sessionVars.php');
    $checkIfStolen = mysqli_query($con, "SELECT * FROM $inventoryTable WHERE $scanCriteria = '$scan' AND ( location = 'STOLEN' OR location = 'MISSING' )");
    if (mysqli_num_rows($checkIfStolen) > 0) {
        return TRUE;         
    }
        else { return FALSE; }
}

//////////////
// SECURITY
//////////////
function reset_auth() {
	require('include/sessionVars.php');	
	if (isset($authorized_groups) OR isset($_SESSION['authorizedGroups'])) {
			unset($_SESSION['authorizedGroups']);
			unset($authorized_groups);
		}
}

function authorize($groupName) {
    require('include/sessionVars.php');	
	$authorized_groups[] = $groupName;
	$_SESSION['authorizedGroups'] = $authorized_groups;
}

function is_authorized() {
    require('include/sessionVars.php');	
	if (isset($authorized_groups)) {
		foreach ($authorized_groups as $authorized) {
			if ($current_group === $authorized) {
				// echo $current_user . '|';
				// echo $authorized . '\n';
				return TRUE;
			}
		}
    }
    return FALSE;
}

function secure_page() {
    require('include/sessionVars.php');	

	// Check that user credentials are there at all
	if (!isset($current_group) OR !isset($current_user)) {
		go_login(0);
		die();
	}
	
	// Check that the current user's group is authorized for this page
	if (!is_authorized()) {
		
		echo '<p><label class="field">' . $current_user . ' is not authorized for this page</label></p>';
			foreach ($authorized_groups as $displayGroup) {
				echo '<p><label class="field">' . $displayGroup . ' is authorized.</label></p>';
				}
			go_home(3);
			die();
		} else {

			if ($current_group === "IT") {
			go_login(300); // Give IT 5 minutes without having to relog
			} else {
			go_login(60); // require other users to relog if page has not changed for 60 seconds
			}
	}
}
