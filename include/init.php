<?php
// Require all necessary support files
require_once('include/initDB.php');
require_once('include/functions.php');
require_once('include/sessionVars.php');

// reset authorized groups so each page can designate its own authorizations
reset_auth();
