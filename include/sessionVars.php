<?php
$authorized_groups = array("IT", "teacher"); // try to fix this later for more readability

if (!isset($_SESSION)) {
	session_start();
}

if (isset($_SESSION['scan'])) {
    $scan = $_SESSION['scan'];
}

if (isset($_SESSION['scanCriteria'])) {
    $scanCriteria = $_SESSION['scanCriteria'];
}

if (isset($_SESSION['locationScan'])) {
    $locationScan = $_SESSION['locationScan'];
}

if (isset($_SESSION['assigneeInput'])) {
    $assigneeInput = $_SESSION['assigneeInput'];
}

if (isset($_SESSION['currentUser'])) {
    $current_user = $_SESSION['currentUser'];
}

if (isset($_SESSION['currentGroup'])) {
    $current_group = $_SESSION['currentGroup'];
}

if (isset($_SESSION['studentName'])) {
    $studentName = $_SESSION['studentName'];
}

if (isset($_SESSION['authorizedGroups'])) {
	$authorized_groups = $_SESSION['authorizedGroups'];
	} else {
		$authorized_groups = NULL;
}

if (isset($_SESSION['massUpdateField'])) {
	$massUpdateField = $_SESSION['massUpdateField'];
}

if (isset($_SESSION['deviceList'])) {
    $deviceList = $_SESSION['deviceList'];
    $deviceCount = count($deviceList);
}
    
if (isset($_SESSION['ignoredScans'])) {
    $ignoredScans = $_SESSION['ignoredScans'];
}