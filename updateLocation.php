<!-- This page takes the location scan from the previous page and updates
     the location in the the inventory database unless the location is
     "STUDENT" or "STAFF", in which case it will ask to whom the device
     is assigned -->
<?php
require_once('include/init.php');
authorize("IT");
authorize("teacher");
secure_page();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>

<?php
$locationScan = strtoupper(filter_input(INPUT_POST, 'locationScan'));
$_SESSION['locationScan'] = $locationScan;

// CHECK LOCATION IS LEGIT
if (!validLocation()) {
    echo "<h2> $locationScan is not a valid Location. </h2>";
    go_home(3);
} else {

// CHECK IF LOCATION IS "STAFF" OR "STUDENT"
// AND IF SO INDICATE ASSIGNEE IS NEEDED
if ($locationScan === "STAFF" || $locationScan === "STUDENT" || $locationScan === "PEF_SUMMER" || $locationScan === "STUDENT_SUMMER") {
    $assigneeNeeded = true;
        } else { $assigneeNeeded = false; }

if ($assigneeNeeded === true) {

    echo '
    <div class="formWrapper">
    <form action="updateAssignee.php" method="post">
    <fieldset>
    <legend>Who is this device going to?: </legend>
        <div class="inventoryForm">
          <p><label class="field">New Assignee: </label>
          <input type="text" name="assigneeInput" autofocus autocomplete="off" /></p>
        </div>
    </fieldset>
    </form>
    </div>';
    
} else {
    if (!mysqli_query($con, "UPDATE $inventoryTable SET location = '$locationScan', assignedTo = NULL WHERE $scanCriteria = '$scan'")) {
    die("Could not update the device to the new location! \nError: " . mysqli_error($con));
    mysqli_free_result($result);
    mysqli_close($con);
    go_home(3);
    } else {
    
    log_change($scanCriteria, $scan);
    
    echo '<div class="formWrapper">
          <form action="" method="">
          <fieldset>
          <legend>Verify the Information: </legend>
            <div class="inventoryForm">
            <p><label class="field">Please Check Info is Correct: </label>
            </div>
          </fieldset>
          </form>';
    
    echo "<h1>New Info for: " . $scan . "</h1>";
    drawTable($scanCriteria, $scan);
    echo '</div>';

    go_home(3);
    }
}
}
?>

</body></html>
