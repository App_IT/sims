<?php
// MAKE THE LIST LOOK NICE - OK
// Button for Commiting Changes
// Scan Code or something to commit changes
// Check for duplicate entries? (not very important)

require_once('include/init.php');
authorize("IT");
secure_page();

?>


<html>
<head>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<title>App Academy Inventory</title>
</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
    
        <div class="formWrapper">
    <form action=' . htmlspecialchars($_SERVER["PHP_SELF"]) . ' method="post">
    <fieldset>
    <legend>Scaning Devices to Update to: <?php echo $massUpdateField ?></legend>
        <div class="inventoryForm">
                <?php
                // if some devices were ignored, remind us of that fact and tell us which
                if (isset($ignoredScans)) {
                    echo '<p class = "error">' . count($ignoredScans) . ' scans were ignored. </p>';
                    echo '<ol>';
                        foreach ($ignoredScans as $ignored) {
                        echo '<li class = "error">A scanned entry, "' . $ignored . '", was ignored</li>';
                        }
                    echo '</ol>';
                }
                // echo '<h4>' . count($deviceList) . ' devices to be updated.</h4>';
                
                $counter = 1;
                $limit = count($deviceList);
                foreach ($deviceList as $updateThis) {
                    // echo '<p>' . $updateThis . '</p>';
                    if (mysqli_query($con, "UPDATE $inventoryTable SET location = '$massUpdateField', assignedTo = NULL WHERE deviceNumber = '$updateThis'")) {
                        
                        // if mysql database is successful, update Change Log
                        log_change("deviceNumber", $updateThis);
                        
                        // if successful, say so and close the connection for the next iteration
                        echo '<p>Device ' . $updateThis . ' (' . $counter . ' of ' . $limit . ') was successfully set to ' . $massUpdateField . '</p>';
                        
                    } else {
                        // mysql query failed for some reason
                        echo '<p class = "error">Could not update device: ' . $updateThis . '</p>';
                        mysqli_close($con); 
                    }
                    $counter++;
                }
                mysqli_close($con); 
                go_home(10);
                ?>
        </div>
        
    </fieldset>
    </form>'
    </div>  
    </body>
</html>