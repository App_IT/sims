<?php
require_once('include/init.php');
authorize("IT");
secure_page();
?>

<html>
<head>
    <title>App Academy Inventory</title> 
    <link rel="stylesheet" type="text/css" href="css/tableStyles.css" />
</head>
<body>
<div class="banner">
    <a href="index.php"><img class="displayed" src="logo.png" /></a>
</div>
        
<?php

		echo '<h2 align="center">Caches</h2>';
		// Show a breakdown of how many machines are in each Swap Station's Cache and Repair sections
		$show_fields = "location, model, diskType, count(*)";
		$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE location LIKE 'ISS_CACHE%' GROUP BY location,model,diskType;");

		if (!$result) {
			die("Query to show fields from table failed");
		}
		
		
		drawTableHorizontal($result);
		mysqli_free_result($result);
		
		$show_fields = "location, model, diskType, count(*)";
		$result = mysqli_query($con, "SELECT $show_fields FROM $inventoryTable WHERE location LIKE 'ISS_REPAIR%' GROUP BY location,model,diskType;");

		if (!$result) {
			die("Query to show fields from table failed");
		}

		
		drawTableHorizontal($result);
		mysqli_free_result($result);
		
		
		echo '<h2 align="center">Repair</h2>';
		// Get all relevant locations (Inventory Swap Stations that aren't empty)
		$ssL = mysqli_query($con, "SELECT DISTINCT location FROM $inventoryTable WHERE location LIKE 'ISS%';");
		while ($row = mysqli_fetch_assoc($ssL)){
			$ssLocations[] = $row['location'];
		}
		mysqli_free_result($ssL);
		
		
		
		
		// Show a breakdown of which machines are in which Swap Station
		$show_details = "deviceNumber, location, model, diskType";
		foreach ( $ssLocations as $station ){
			$details = mysqli_query($con, "SELECT $show_details FROM $inventoryTable WHERE location = '$station' ORDER BY location,model,diskType,deviceNumber;");
			if (!$details){
				die(mysqli_error($con));
			}
			
			drawTableHorizontal($details);
			mysqli_free_result($details);
			echo '<h2 align="center">Details for ' . $station . '</h2>';
		}
?>
</body>
</html>
