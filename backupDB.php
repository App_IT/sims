<?php
require_once('include/init.php');
authorize("IT");
secure_page();
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/styles.css" />
		<title>App Academy Inventory</title>
	</head>
<body>
    <div class="banner">
        <a href="index.php"><img class="logo" src="logo.png" /></a>
    </div>
    
<?php
	$output = shell_exec("/etc/scripts/mysql_db_backup/backup_mysql.sh 2>&1");
	echo '<br />';
	echo '<h3>' . $output . '</h3>';
	go_home(5);
?>

</body>
</html>